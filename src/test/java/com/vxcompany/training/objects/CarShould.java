package com.vxcompany.training.objects;

import org.junit.Test;

import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

// this class is a test suite. every @Test method is executed as a separate test within this suite.
public class CarShould
{
	// this is a single test. FIRST principles apply.
	@Test
	public void accelerateOnlyWhenShiftedUp()
	{
		// given
		Car carUnderTest = new Car();

		// when
		carUnderTest.shiftUp();
		carUnderTest.accelerate();

		// then
		assertThat( carUnderTest.getSpeed(), is( greaterThan( 0 ) ) );
	}
}