package com.vxcompany.training.primitives;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class OperatorsTest
{
	@Test
	public void threePlusFiveShouldBeEight()
	{
		// replace the zero below by three plus five (the sum, not the answer).
		assertEquals( 8, 0 );
	}

	@Test
	public void fourMinusThreeShouldBeThree()
	{
		// replace the zero below by four minus three (the sum, not the answer).
		assertEquals( 1, 0 );
	}

	@Test
	public void twoTimesSixIsTwelve()
	{
		// replace the zero below by two times six (the sum, not the answer).
		assertEquals( 12, 0 );
	}

	@Test
	public void nineDividedByThreeIsThree()
	{
		// replace the zero below by nine divided by three (the sum, not the answer).
		assertEquals( 3, 0 );
	}

	@Test
	public void sevenDividedByThreeIs_something()
	{
		// replace the zero below by seven divided by three (the sum, not the answer).
		// replace the one below by the expected answer of this division
		// adjust the name of the test method accordingly
		// keep in mind: division on two integers cuts away the remainder
		assertEquals( 0, 1 );
	}

	@Test
	public void print7Modulo3()
	{
		// a modulo of two integers is the remainder of the division of those integers
		// replace the zero below by seven modulo three (the sum, not the answer).
		// replace the one below by the expected answer of this division
		assertEquals( 0, 1 );
	}
}