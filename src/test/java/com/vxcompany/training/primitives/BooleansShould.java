package com.vxcompany.training.primitives;

import com.vxcompany.training.objects.Car;
import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class BooleansShould
{
	@Test
	public void threeIsGreaterThanTwo()
	{
		// replace '0' by the boolean expression 'three is greater than two'
		assertThat( 0, is( true ) );
	}

	@Test
	public void threeIsNotGreaterThanFive()
	{
		// replace '0' by the boolean expression 'three is equal to three'
		assertThat( 0, is( false ) );
	}

	@Test
	public void threeIsEqualToThree()
	{
		// replace '0' by the boolean expression 'three is equal to three'
		assertThat( 0, is( true ) );

	}

	@Test
	public void threeIsNotEqualToFive()
	{
		// replace '0' by the boolean expression 'three is not equal to five'
		assertThat( 0, is( true ) );
	}

	@Test
	public void threeIsEqualToFive()
	{
		// replace '0' by the boolean expression 'three is equal to five'
		assertThat( 0, is( false ) );
	}

	@Test
	public void carBecomesInaccessible()
	{
		Car car = new Car();
	}
}