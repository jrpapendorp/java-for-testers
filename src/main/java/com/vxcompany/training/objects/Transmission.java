package com.vxcompany.training.objects;

class Transmission
{
	private final int maxGears;

	private int currentGear;

	Transmission( int maxGears )
	{
		this.maxGears = maxGears;
		this.currentGear = 0;
	}

	void shiftUp()
	{
		if( currentGear < maxGears )
		{
			currentGear++;
		}
	}

	void shiftDown()
	{
		if( currentGear > -1 )
		{
			currentGear--;
		}
	}

	int translate( int acceleration )
	{
		return acceleration * currentGear;
	}
}
