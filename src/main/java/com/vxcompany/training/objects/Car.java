package com.vxcompany.training.objects;

import static java.lang.String.valueOf;

public class Car
{
	private static int lastPlate = 0;

	private int speed;
	private int gear;
	private final String licensePlate;

	// The constructor. This is called when the object is created.
	public Car()
	{
		this.speed = 0;
		this.gear = 0;

		this.licensePlate = registerNewLicencePlate();
	}

	private static String registerNewLicencePlate()
	{
		lastPlate += 1;
		return valueOf( lastPlate );
	}

	public void accelerate( int acceleration )
	{
		speed += acceleration * gear;
	}

	public void shiftUp()
	{
		if( gear < 5 )
		{
			gear++;
		}
	}

	public void shiftDown()
	{
		if( gear > -1 )
		{
			gear--;
		}
	}

	public int getSpeed()
	{
		return speed;
	}

	public void decelerate()
	{
		if( speed < 20 )
		{
			speed = 0;
		}
		else
		{
			speed -= 20;
		}
	}

	public String getLicensePlate()
	{
		return licensePlate;
	}
}
